﻿using OCP_Specifications.Specifications;
using System.Linq;

namespace OCP_Specifications.Filters
{
    public interface IFilter<T>
    {
        IQueryable<T> Filter(IQueryable<T> data, ISpecification<T> spec);
    }
}
