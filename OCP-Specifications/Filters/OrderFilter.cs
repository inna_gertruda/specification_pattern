﻿using OCP_Specifications.Specifications;
using System.Linq;

namespace OCP_Specifications.Filters
{
    public class OrderFilter : IFilter<Order>
    {
        public IQueryable<Order> Filter(IQueryable<Order> data, ISpecification<Order> spec)
        {
            return data.Where(x=>spec.IsSatisfiedBy(x));
        }
    }
}
