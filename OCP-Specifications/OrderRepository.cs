﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace OCP_Specifications
{
    internal class OrderRepository
    {
        private readonly List<Order> Orders = new List<Order>
        {
            new Order()
            {
                Title = "Furnishing apartment",
                OrderDate = new DateTime(2018, 1, 4),
                TotalPrice = 61m,
                Items = new List<OrderItem>{
                    new OrderItem()
                    {
                        Title = "Vase",
                        Price = 10.3m,
                        Category = Category.Decor
                    },
                    new OrderItem()
                    {
                        Title = "Chair",
                        Price = 20.5m,
                        Category = Category.Furniture
                    },
                    new OrderItem()
                    {
                        Title = "Sofa",
                        Price = 30.2m,
                        Category = Category.Furniture
                    }
                }
            },
            new Order()
            {
                Title = "Cooking desert",
                OrderDate = new DateTime(2019, 5, 24),
                TotalPrice = 260.6m,
                Items = new List<OrderItem>{
                    new OrderItem()
                    {
                        Title = "Dough",
                        Price = 67.5m,
                        Category = Category.Food
                    },
                    new OrderItem()
                    {
                        Title = "Jam",
                        Price = 12.4m,
                        Category = Category.Food
                    },
                    new OrderItem()
                    {
                        Title = "Beautiful can",
                        Price = 180.7m,
                        Category = Category.Decor
                    }
                }
            },
            new Order()
            {
                Title = "Friday evening",
                OrderDate = new DateTime(2020, 12, 31),
                TotalPrice = 120m,
                Items = new List<OrderItem>{
                    new OrderItem()
                    {
                        Title = "Chair",
                        Price = 60m,
                        Category = Category.Furniture
                    },
                    new OrderItem()
                    {
                        Title = "Chips",
                        Price = 60m,
                        Category = Category.Food
                    }
                }
            }
        };

        public OrderRepository()
        {
        }

        internal IQueryable<Order> GetData()
        {
            return Orders.AsQueryable();
        }
    }
}