﻿using System;

namespace OCP_Specifications.Specifications
{
    public class DateGreaterSpecification : BaseSpecification<Order>
    {
        private readonly DateTime Date;

        public DateGreaterSpecification(DateTime date)
        {
            Date = date;
        }

        public override bool IsSatisfiedBy(Order candidate)
        {
            return candidate.OrderDate > Date;
        }
    }
}
