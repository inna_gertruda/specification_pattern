﻿using System.Collections.Generic;
using System.Linq;

namespace OCP_Specifications.Specifications
{
    public class HasCategorySpecification : BaseSpecification<Order>
    {
        private readonly List<Category> Categories;

        public HasCategorySpecification(List<Category> categories)
        {
            Categories = categories;
        }

        public override bool IsSatisfiedBy(Order candidate)
        {
            return candidate.Items.Any(x => Categories.Contains(x.Category));
        }
    }
}
