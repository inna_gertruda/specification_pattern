﻿namespace OCP_Specifications.Specifications
{
    public class PriceGreaterSpecification : BaseSpecification<Order>
    {
        private readonly decimal Price;

        public PriceGreaterSpecification(decimal price)
        {
            Price = price;
        }

        public override bool IsSatisfiedBy(Order candidate)
        {
            return candidate.TotalPrice > Price;
        }
    }
}
