﻿namespace OCP_Specifications.Specifications
{
    public interface ISpecification<T>
    {
        bool IsSatisfiedBy(T candidate);
    }
}
