﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OCP_Specifications.Specifications.Abstraction
{
    public class AndSpecification<T> : BaseSpecification<T>
    {
        private readonly ISpecification<T> _leftCondition;
        private readonly ISpecification<T> _rightCondition;

        public AndSpecification(ISpecification<T> left, ISpecification<T> right)
        {
            _leftCondition = left;
            _rightCondition = right;
        }

        public override bool IsSatisfiedBy(T candidate)
        {
            return _leftCondition.IsSatisfiedBy(candidate) && _rightCondition.IsSatisfiedBy(candidate);
        }
    }
}
