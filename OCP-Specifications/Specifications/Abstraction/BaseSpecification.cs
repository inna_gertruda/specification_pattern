﻿using OCP_Specifications.Specifications.Abstraction;

namespace OCP_Specifications.Specifications
{
    public abstract class BaseSpecification<T> : ISpecification<T>
    {
        public abstract bool IsSatisfiedBy(T candidate);
       
        public static BaseSpecification<T> operator &(BaseSpecification<T> spec1, BaseSpecification<T> spec2)
        {
            return new AndSpecification<T>(spec1, spec2); ;
        }

        public static BaseSpecification<T> operator |(BaseSpecification<T> spec1, BaseSpecification<T> spec2)
        {
            return new OrSpecification<T>(spec1, spec2); ;
        }
    }
}
