﻿namespace OCP_Specifications.Specifications
{
    internal class OrSpecification<T> : BaseSpecification<T>
    {
        private readonly ISpecification<T> _leftCondition;
        private readonly ISpecification<T> _rightCondition;

        public OrSpecification(ISpecification<T> leftCondition, ISpecification<T> rightCondition)
        {
            _leftCondition = leftCondition;
            _rightCondition = rightCondition;
        }

        public override bool IsSatisfiedBy(T candidate)
        {
            return _leftCondition.IsSatisfiedBy(candidate) || _rightCondition.IsSatisfiedBy(candidate);
        }
    }
}