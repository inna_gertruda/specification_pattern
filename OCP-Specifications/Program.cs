﻿using OCP_Specifications.Extensions;
using OCP_Specifications.Filters;
using OCP_Specifications.Specifications;
using System;
using System.Collections.Generic;
using System.Linq;

namespace OCP_Specifications
{
    class Program
    {
        static void Main(string[] args)
        {
            var dataRepository = new OrderRepository();
            var orderFilter = new OrderFilter();

            //Case: all data
            var data = dataRepository.GetData();
            var dateSpec = new DateGreaterSpecification(new DateTime(2018, 1, 1));
            var categorySpec = new HasCategorySpecification(new List<Category> { Category.Furniture, Category.Food, Category.Decor});
            var priceSpec = new PriceGreaterSpecification(10m);

            data = orderFilter.Filter(data, (priceSpec | dateSpec) & categorySpec);
            data.Display("All Data");

            //Case: Food || Date
            data = dataRepository.GetData();
            dateSpec = new DateGreaterSpecification(new DateTime(2020, 1, 1));
            categorySpec = new HasCategorySpecification(new List<Category> { Category.Food });

            data = orderFilter.Filter(data, categorySpec | dateSpec);
            data.Display($"Food || {new DateTime(2020, 1, 1)}");

            //Case: Decor && Date
            data = dataRepository.GetData();
            dateSpec = new DateGreaterSpecification(new DateTime(2019, 1, 1));
            categorySpec = new HasCategorySpecification(new List<Category> { Category.Decor });

            data = orderFilter.Filter(data, categorySpec & dateSpec);
            data.Display($"Decor && {new DateTime(2019, 1, 1)}");

            Console.WriteLine(String.Empty);
        }
    }
}
