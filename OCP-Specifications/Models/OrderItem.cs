﻿namespace OCP_Specifications
{
    public class OrderItem
    {
        public string Title { get; set; }
        public decimal Price { get; set; }
        public Category Category { get; set; }
    }

    public enum Category
    {
        Decor,
        Furniture,
        Food
    }
}