﻿using System;
using System.Collections.Generic;

namespace OCP_Specifications
{
    public class Order
    {
        public string Title { get; set; }
        public DateTime OrderDate { get; set; }
        public decimal TotalPrice { get; set; }
        public IEnumerable<OrderItem> Items { get; set; }
    }
}