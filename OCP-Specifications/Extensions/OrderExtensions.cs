﻿using System;
using System.Linq;

namespace OCP_Specifications.Extensions
{
    public static class OrderExtensions
    {
        public static void Display(this IQueryable<Order> orders, string title)
        {
            Console.BackgroundColor = ConsoleColor.DarkBlue;
            Console.WriteLine(title);
            Console.ResetColor();
            orders.ToList().ForEach(order =>
            {
                Console.WriteLine($"{order.Title}:{order.OrderDate} - {order.TotalPrice}");
                order.Items
                    .ToList()
                    .ForEach(item => Console.WriteLine($"\t{item.Category}:{item.Title} - {item.Price}"));
            });
            Console.WriteLine("---------------------------------");
        }
    }
}
